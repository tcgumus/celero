<?php 		
	$this->load->view('template/header');
?>
<div class="container">
	<div class="well">
		<p><b>What we do </b></p>

		<p>CELERO is a license-free web-based software to support decision making for companies and other stakeholders aiming to implement resource efficiency projects such as Cleaner Production and Industrial Symbiosis projects.[EVU1]</p>

		<p>CELERO allows: </p>

		<p>- to collect, store, manage and communicate information on material and energy flows,</p>

		<p>- to detect optimization scenarios through cost/benefit analysis, </p>

		<p>- social networking between stakeholders and experts,</p>

		<p>- and dissemination through an open-access knowledge repository.</p>

		<p>For more information on CELERO functionalities, check out "<i><a style="color: #34495e" href="<?php echo base_url('functionalities'); ?>"><?php echo lang("functionalities");?></a></i>".</p>

		<p><b>Partners</b></p>

		<p>- Institute for Ecopreneurship, IEC School of Life Sciences, HLS - FHNW (Switzerland).</p>

		<p>- University of Economics and Technology (Turkey)</p>

		<p>- Sofies SA (Switzerland)</p>

		<p>- ODAGEM A.S. (Turkey)</p>

		<p>- Industrial ecology group of the University of Lausanne (Switzerland)</p>
	</div>
</div>
<?php
	$this->load->view('template/footer'); 
?>